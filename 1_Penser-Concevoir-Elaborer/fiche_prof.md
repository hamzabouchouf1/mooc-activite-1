## fiche prof pour une activité

**Thématique :** Module N°1 Généralités sur les systèmes informatiques 

**Notions liées :** information, traitement, informatique, système informatique, périphérique, logiciel, application, ordinateur. 

**Résumé de l’activité :** simuler le processus d'assemblage les composants d'un ordinateur.

**Objectifs :** mettre en pratique un cas d'usage d'assemblage un nouveau ordinateur, découvrir les différents composants d'un ordinateur et leur rôle.

**Auteur :** Hamza Bouchouf

**Durée de l’activité :** une heure (après la prise de connaissance du cours).

**Forme de participation :** en groupe.

**Matériel nécessaire :** des ordinateurs non assemblés (ordinateur par groupe)

**Préparation :** préparer des ordinateurs non assemblés, avec des tournevis.

### Références:

**Fiche élève cours :** https://informatique-tuto.com/wp-content/uploads/2021/01/M1_chap-2.pdf

**Fiche élève activité :** https://docs.google.com/document/d/1bD8kX_m56BNUlcYK86qIXpKFQ0MwOs4llBA6TsSLJFs/edit?usp=sharing
